var black = '██';
var white = '  ';
var line = '';

function drawline1() {
  for (var i = 1; i < 10; i++){
    if(i % 2 === 0){
      line += black;
    } else{
      line += white;
    }
  }
}

function drawline2() {
  for (var j = 1; j < 10; j++){
    if(j % 2 === 0){
      line += white;
    } else{
      line += black;
    }
  }
}

function drawVertical() {
  for (var j = 1; j < 8; j++){
    if(j % 2 === 0){
      drawline1();
      line += '\n';
    } else{
      drawline2();
      line += '\n';
    }
  }
}

drawVertical();
console.log(line);